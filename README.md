# README #

### What is this repository for? ###

* Sample project containing a sample Bitbucket for Workflows workflow.
* This sample shows how to incorporate License and Copyright analysis into your current workflow.
* Version 1.9

### How do I get set up? ###

* See file: Getting Started w/LiAnORT and Bitbucket Pipelines v1.9

### Who do I talk to? ###

* Send questions or comments to charlie@canvasslabs.com